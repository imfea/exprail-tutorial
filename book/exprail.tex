\documentclass[a4paper]{article}

% Set margins
\usepackage[hmargin=2.5cm, vmargin=3cm]{geometry}

\frenchspacing

% Language packages
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% AMS
\usepackage{amssymb,amsmath}

% Graphic packages
\usepackage{graphicx}

% Colors
\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}

% Enumeration
\usepackage{enumitem}

% Python
\usepackage{python}
\usepackage{listings}

\begin{document}

\begin{center}
    \Huge \textbf{Exprail Tutorial}
   
	\medskip   
   
    \huge Imre Piller
\end{center}

\bigskip

\tableofcontents

\section{Introduction}

The name \textit{Exprail} is the abbreviated form of \textit{Expression Rails}. It helps to define expressions as finite state machines. We can draw the state transition diagram (which is also called \textit{syntax diagram} or \textit{railroad diagram}), define the set of input tokens and the graph interpreter of the Exprail library can parse the grammar.

\subsection{Sources and Tokens}

The Exprail uses a more generalized way of parsing. It uses tokens as inputs and outputs. A token is a pair of \textbf{type} and \textbf{value}. The type and the value can be an arbitrary string.

The \textit{source} is the source of the tokens. We can require a token from it. We can imagine the source as the infinite stream of tokens. When we model a finite length stream (for example a file) we have to use \textbf{empty token}. We must define the empty token. It can be for example
\begin{python}
{'type': 'empty', 'value': ''}
\end{python}

Let see an example when we would like to tokenize words in a text. Let see the text "\texttt{It is a short text}". Let check an immediate state of the parsing process. The inputs of the parser are the followings.
\begin{python}
{'type': 'char', 'value': 'I'}
{'type': 'char', 'value': 't'}
{'type': 'char', 'value': ' '}
{'type': 'char', 'value': 'i'}
{'type': 'char', 'value': 's'}
{'type': 'char', 'value': ''}
{'type': 'char', 'value': 'a'}
{'type': 'char', 'value': ''}
\end{python}
The parser provides the following output tokens.
\begin{python}
{'type': 'word', 'value': 'It'}
{'type': 'word', 'value': 'is'}
{'type': 'word', 'value': 'a'}
\end{python}

We can see a graphical representation of the processing on Figure \ref{fig:parsing}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{images/parsing.png}
\caption{The parsing process}
\label{fig:parsing}
\end{figure}

% TODO: Tokenization vs Parsing

\subsection{Simple Grammars}

Any grammar must contains exactly one \textit{start} node and at least one \textit{finish} node. The start node is the entry point of the parsing and the finish node is the exit point. When we link them with an edge we get a valid grammar graph (Figure \ref{fig:none_grammar}).

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{images/none_grammar.png}
\caption{A grammar diagram which accepts any token}
\label{fig:none_grammar}
\end{figure}

Let assume that we would like to check the token which has been processed. We can extend the previous grammar by an \textit{information} node (Figure \ref{fig:info_grammar}). The information node does not change the state of the stream, it just read the current token.

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{images/info_grammar.png}
\caption{A classical \emph{Hello, World!} example}
\label{fig:info_grammar}
\end{figure}

The grammar definition file (\texttt{hello.grammar}) looks like the following:
\begin{python}
expression "hello"
nodes
1 start "" 100 100
2 finish "" 300 100
3 info "Hello!" 200 100
edges
1 3
3 2
\end{python}

For using Exprail we have to
\begin{itemize}
\item load the grammar from a file, and set the appropriate classifier,
\item define our parser class which is derived from the \texttt{exprail.parser.Parser} class,
\item set the source for the input token stream.
\end{itemize}

In this case the classifier is not necessary, it can be \texttt{None}. For the \textit{information} node we have to override the \texttt{show\_info} method of the \texttt{Parser} class. The source can be a predefined \texttt{SourceString} which makes available the given string character-by-character.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

class HelloParser(Parser):
    """Hello Exprail parser class"""
    
    def show_info(self, message, token):
        print('{}: "{}" ({})'.format(message, token.value, token.type))

grammar = Grammar(filename='hello.grammar', classifier=None)
source = SourceString('abc')
hello_parser = HelloParser(grammar, source)
hello_parser.parse()
\end{python}

The source code above results the following output:
\begin{python}
Hello!: "a" (char)
\end{python}

The helping of precise and verbose error handling is a key part of the Exprail. The \textit{error} node works similarly to the \textit{information} node, except it stops the parsing process. Let see a minimal example on Figure \ref{fig:error_grammar}!

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{images/error_grammar.png}
\caption{Minimal example with an error node}
\label{fig:error_grammar}
\end{figure}

The \texttt{Parser} class has a default implementation for printing error to the standard output. Therefore, we can define our \texttt{ErrorParser} class as the following.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

class ErrorParser(Parser):
    """Parser class for displaying error message"""
    
    pass

grammar = Grammar(filename='error.grammar', classifier=None)
source = SourceString('abc')
error_parser = ErrorParser(grammar, source)
error_parser.parse()
\end{python}
It results the following output:
\begin{python}
ERROR: Invalid!
\end{python}

For checking the tokens and making decisions according to its type and value, we can use the \textit{token} node.

As an example, define a grammar which validate that the input stream is empty (Figure \ref{fig:empty_grammar})!

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{images/empty_grammar.png}
\caption{A grammar which accepts only the empty source}
\label{fig:empty_grammar}
\end{figure}

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    if token_class == 'empty':
        if token.type == 'empty' and token.value == '':
            return True
    return False

class EmptyParser(Parser):
    """Parser class for empty sources"""
    
    pass

grammar = Grammar(filename='empty.grammar', classifier=is_in_class)
source = SourceString('')
empty_parser = EmptyParser(grammar, source)
empty_parser.parse()
\end{python}

Let define a grammar and create a parser which accepts only one character token! We can see the corresponding grammar definition on the Figure \ref{fig:once_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=1]{images/once_grammar.png}
\caption{A grammar which accepts exactly one character from the source}
\label{fig:once_grammar}
\end{figure}

We can parse an arbitrary string against the defined grammar by the following source code.
\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    return token_class == token.type

grammar = Grammar(filename='once.grammar', classifier=is_in_class)
source = SourceString('')
once_parser = Parser(grammar, source)
once_parser.parse()
\end{python}

It signs error on empty string and multiple characters.
(We have instantiated the \texttt{Parser} class directly, because in this case the customization is not necessary.)

\subsection{Operations}

We can define custom actions at certain points of the syntax graph by \textit{operation} nodes.

Let see an example which increment a counter by \texttt{+} characters and decrement by \texttt{-} characters. Let sign the \texttt{?} character the end of the counting.

We can see the grammar definition on Figure \ref{fig:counter_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.9]{images/counter_grammar.png}
\caption{A grammar for increment and decrement a counter}
\label{fig:counter_grammar}
\end{figure}

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    return token_class == token.value

class CounterParser(Parser):
    """Parser class for counting"""
    
    def __init__(self, grammar, source):
        super(CounterParser, self).__init__(grammar, source)
        self._counter = 0

    def operate(self, operation, token):
        _ = token
        if operation == 'increase':
            self._counter += 1
        elif operation == 'decrease':
            self._counter -= 1
        elif operation == 'show':
            print('The value of the counter is {}.'.format(self._counter))
        else:
            raise ValueError('The operation {} is invalid!'.format(operation))

grammar = Grammar(filename='counter.grammar', classifier=is_in_class)
source = SourceString('+-++++--+?...')
counter_parser = CounterParser(grammar, source)
counter_parser.parse()
\end{python}
For customizing the operation we have to override the default implementation of the \texttt{operate} method.

\subsection{Stacks}

In many cases we have to collect the results of the parsing process. The Exprail provides the \textit{stack} and the \textit{clean} nodes for making it easier.

Let create a parser which tokenize the words of a text! In the first case let assume that the words have separated by spaces.

We can see the grammar definition on Figure \ref{fig:words_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{images/words_grammar.png}
\caption{A grammar for tokenizing words}
\label{fig:words_grammar}
\end{figure}

The white square nodes are \textit{token} nodes. They have distinguished, because the alphabetic and whitespace classes are common.

The green node with negation sign is an \textit{except token} node, which use the negated condition of the \textit{token} node.

The processing has finished after any invalid token (for example the empty token).

We can use the defined grammar by the following source code.
\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    if token_class == 'a-Z':
        return token.value.isalnum()
    elif token_class == 'ws':
        return token.value == ' '
    else:
        raise ValueError('Unexpected token class! "{}"'.format(token_class))

class WordParser(Parser):
    """Parser class for word tokenization"""
    
    def __init__(self, grammar, source):
        super(WordParser, self).__init__(grammar, source)
        self._words = []

    @property
    def words(self):
        return self._words

    def operate(self, operation, token):
        if operation == 'save':
            word = ''.join(self._stacks[''])
            self._words.append(word)
        else:
            raise ValueError('Invalid operation!')

grammar = Grammar(filename='words.grammar', classifier=is_in_class)
source = SourceString('It is a short text')
word_parser = WordParser(grammar, source)
word_parser.parse()
print(word_parser.words)
\end{python}
The \texttt{Parser} class has a \texttt{\_stacks} member which manages stacks by names. There is a default stack which name is an empty string. In this case the stack nodes have empty value.

\subsection{Expressions}

The expression is the main building block of Exprail. In fact, we define expressions by graphs, which can refer to other graphs by the \texttt{expression} typed nodes.

Let see an example, where we define grammar for the list of integers! We can see the definition of integer literals on Figure \ref{fig:integer_grammar} and the list of integers on Figure \ref{fig:intlist_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/integer_grammar.png}
\caption{A grammar of the natural number literals}
\label{fig:integer_grammar}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/intlist_grammar.png}
\caption{A grammar of integer list}
\label{fig:intlist_grammar}
\end{figure}

We can used these grammars for example in the following way.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    if len(token_class) == 1:
        return token_class == token.value
    elif token_class == 'space':
        return token.value == ' '
    elif token_class == '0-9':
        return token.value in '0123456789'
    elif token_class == '1-9':
        return token.value in '123456789'
    else:
        raise ValueError('Invalid token class or token!')

class ListParser(Parser):
    """Parser class for list of integers"""

    def __init__(self, grammar, source):
        super(ListParser, self).__init__(grammar, source)
        self._values = []
    
    def operate(self, operation, token):
        if operation == 'append':
            number = ''.join(self._stacks[''])
            self._values.append(int(number))
        elif operation == 'show':
            print('Values: {}'.format(self._values))
        else:
            raise ValueError('Unexpedted operation!')

grammar = Grammar(filename='intlist.grammar', classifier=is_in_class)
source = SourceString('[5, 1, 88888888, 1234, 765, 3]')
list_parser = ListParser(grammar, source)
list_parser.parse()
\end{python}

It worth to note that the \texttt{integer} grammar expects character from the set \texttt{1-9}. The \texttt{intlist} grammar is able to handle all possible character. When we embed expressions to expressions we have to consider these slight differences. We can imagine the expression node as a jump point for the referred expression. The router checks the internals of the expression but the rules of routing remains the same.

\section{CSV Parser}

The CSV (\textit{Comma Separated Values}) is a common data format but has not standardized. We show a simple CSV parser which can be customized for specific needs.

Let assume that we use the semicolon (\texttt{;}) as the separator character. We would like to store keywords (text values without spaces) and string literals. We can see the definition of the \texttt{keyword} grammar on Figure \ref{fig:keyword_grammar}, the \texttt{string} grammar on Figure \ref{fig:string_grammar} and the \texttt{csv} grammar on Figure \ref{fig:csv_grammar}. (The string has simplified in the sense that it does not handle all of the commonly used escaped characters.)

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/keyword_grammar.png}
\caption{Grammar of simple alphanumeric words}
\label{fig:keyword_grammar}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{images/string_grammar.png}
\caption{Simplified string literal grammar}
\label{fig:string_grammar}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/csv_grammar.png}
\caption{Specific CSV format}
\label{fig:csv_grammar}
\end{figure}

The following source code shows a possible use case.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString

def is_in_class(token_class, token):
    if len(token_class) == 1:
        return token_class == token.value
    elif token_class == 'newline':
        return token.value == '\n'
    elif token_class == 'empty':
        return token.type == 'empty' and token.value == ''
    elif token_class == 'alphanum':
        return token.value.isalnum()
    else:
        raise ValueError('Invalid token class or token!')

class CsvParser(Parser):
    """Parser class for a specific CSV file format"""

    def __init__(self, grammar, source):
        super(CsvParser, self).__init__(grammar, source)
        self._table = []
        self._row = []
    
    def operate(self, operation, token):
        if operation == 'keyword':
            keyword = ''.join(self._stacks[''])
            self._row.append(keyword)
        elif operation == 'string':
            string = ''.join(self._stacks[''])
            self._row.append(string)
        elif operation == 'append':
            self._table.append(self._row)
            self._row = []
        elif operation == 'show':
            print('Table: {}'.format(self._table))
        else:
            raise ValueError('Unexpected operation!')

grammar = Grammar(filename='csv.grammar', classifier=is_in_class)
source = SourceString('abc;123;"It works!";\nOnly;for;"testing purposes!";')
csv_parser = CsvParser(grammar, source)
csv_parser.parse()
\end{python}

There are many great CSV libraries. This example shows the capabilities of the Exprail framework, but usually it is not a good idea to write CSV parser in this way. The Exprail has benefits, when we have many special column types and the grammar is more complicated.

\section{JSON Parser}

We can found the syntax diagrams of the JSON language on \texttt{json.org}. We can draw a slightly modified version of these diagrams, and after the definition of sets we can parse the JSON format.

Instead of the direct rewrite of the language definition, it is a better approach to separate the processing to tokenization and parsing. At first, we create a tokenizer.

\subsection{Tokenizer}

The tokenizer waits the stream of input characters and results a higher level tokens. We can see the grammar of the tokenizer on the Figure \ref{fig:json_tokenizer_grammar}. We distinguish the \textit{formatter}, \textit{string}, \textit{number} and \textit{keyword} types. The whitespace characters (between the tokens) have simply ignored.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/json_tokenizer_grammar.png}
\caption{JSON \texttt{tokenizer}}
\label{fig:json_tokenizer_grammar}
\end{figure}

The formatters are the special delimiter characters of the JSON language. Practically, it can be replaced by a set. We can see its grammar on the Figure \ref{fig:json_formatter_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.9]{images/json_formatter_grammar.png}
\caption{JSON \texttt{formatter}}
\label{fig:json_formatter_grammar}
\end{figure}

We have implemented a slightly simplified version of the strings (Figure \ref{fig:json_string_grammar}). It means that, this definition does not handle unicode characters which have given in hexadecimal form.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.55]{images/json_string_grammar.png}
\caption{JSON \texttt{string}}
\label{fig:json_string_grammar}
\end{figure}

The definition of floating point numbers directly rewritten from the definition (Figure \ref{fig:json_number_grammar}).

\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{images/json_number_grammar.png}
\caption{JSON \texttt{number}}
\label{fig:json_number_grammar}
\end{figure}

For \texttt{null}, \texttt{true} and \texttt{false} keywords we have defined the \texttt{keyword} grammar (Figure \ref{fig:json_keyword_grammar}).

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{images/json_keyword_grammar.png}
\caption{JSON \texttt{keyword}}
\label{fig:json_keyword_grammar}
\end{figure}

We can use these definitions by the following source code.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser
from exprail.source import SourceString
from exprail.token import Token
\end{python}

\begin{python}
def is_in_class(token_class, token):
    if len(token_class) == 1:
        return token_class == token.value
    elif token_class == 'whitespace':
        return token.value in [' ', '\n', '\t']
    elif token_class == '0-9':
        return token.value.isdigit()
    elif token_class == '1-9':
        return token.value.isdigit() and token.value != '0'
    elif token_class == 'a-z':
        return token.value.islower()
    elif token_class == 'empty':
        return token.type == 'empty' and token.value == ''
    else:
        raise ValueError('Invalid token class or token!')
\end{python}

\begin{python}
class JsonTokenizer(Parser):
    """JSON tokenizer class"""
    
    def __init__(self, grammar, source):
        super(JsonTokenizer, self).__init__(grammar, source)

    def operate(self, operation, token):
        if operation == 'formatter':
            self._token = Token('formatter', token.value)
            self._ready = True
        elif operation in ['string', 'number', 'keyword']:
            value = ''.join(self._stacks[''])
            self._token = Token(operation, value)
            self._ready = True
        elif operation == 'escaped':
            escaped_chars = {
                'b': '\b', 'f': '\f', 'n': '\n', 'r': '\r', 't': '\t'
            }
            if token.value in excaped_chars:
                self._stacks[''].append(escaped_chars[token.value])
            elif token.value in ['"', '\\', '/']:
                self._stacks[''].append(token.value)
        else:
            raise ValueError('The operation name {} has not defined!'
                             .format(operation))
\end{python}

\begin{python}
grammar = Grammar(filename='json_tokenizer.grammar', classifier=is_in_class)
source = SourceString('{"first": 0.1234, "second": [1, 2, 3, null]}')
json_tokenizer = JsonTokenizer(grammar, source)
\end{python}

\subsection{Parsers as Sources}

We can link parsers to each others for multi-level parsing. For testing purposes we can define a simple grammar which dumps the content of the parser as a source. We can see its definition on Figure \ref{fig:dump_grammar}.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/dump_grammar.png}
\caption{JSON \texttt{keyword}}
\label{fig:dump_grammar}
\end{figure}

We can use it the following form.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser

def is_in_class(token_class, token):
    if token_class == 'empty':
        return token.type == 'empty' and token.value == ''
    else:
        raise ValueError('Unexpected token class!')

grammar = Grammar(filename='dump.grammar', classifier=is_in_class)
dump_parser = Parser(grammar, json_tokenizer)
dump_parser.parse()
\end{python}

It resulted the following output.

\begin{verbatim}
INFO: info, formatter::"{"
INFO: info, string::"first"
INFO: info, formatter::":"
INFO: info, number::"0.1234"
INFO: info, formatter::","
INFO: info, string::"second"
INFO: info, formatter::":"
INFO: info, formatter::"["
INFO: info, number::"1"
INFO: info, formatter::","
INFO: info, number::"2"
INFO: info, formatter::","
INFO: info, number::"3"
INFO: info, formatter::","
INFO: info, keyword::"null"
INFO: info, formatter::"]"
INFO: info, formatter::"}"
INFO: info, empty::""
\end{verbatim}

\subsection{Parser}

For the JSON parser it is enough to define the object (Figure \ref{fig:json_object_grammar}), array (Figure \ref{fig:json_array_grammar}) and value (Figure \ref{fig:json_value_grammar}) grammars.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{images/json_object_grammar.png}
\caption{JSON \texttt{object}}
\label{fig:json_object_grammar}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/json_array_grammar.png}
\caption{JSON \texttt{array}}
\label{fig:json_array_grammar}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{images/json_value_grammar.png}
\caption{JSON \texttt{value}}
\label{fig:json_value_grammar}
\end{figure}

We can implement the parser by using a value stack.

\begin{python}
from exprail.grammar import Grammar
from exprail.parser import Parser

def is_in_class(token_class, token):
    if len(token_class) == 1:
        return token.value == token_class
    elif token_class in ['string', 'number']:
        return token.type == token_class
    elif token_class in ['true', 'false', 'null']:
        return token.type == 'keyword' and token.value == token_class
    elif token_class == 'empty':
        return token.type == 'empty' and token.value == ''
    else:
        raise ValueError('Unexpected token class!')

class JsonParser(Parser):
    """JSON parser class"""
    
    def __init__(self, grammar, source):
        super(JsonParser, self).__init__(grammar, source)
        
        self._value_stack = []

    @property
    def result(self):
        return self._value_stack[-1]

    def operate(self, operation, token):
        if operation == 'create_object':
            self._value_stack.append({})
        elif operation == 'set_key':
            self._value_stack.append(token.value)
        elif operation == 'add_pair':
            value = self._value_stack.pop()
            key = self._value_stack.pop()
            self._value_stack[-1][key] = value
        elif operation == 'create_array':
            self._value_stack.append([])
        elif operation == 'add_item':
            item = self._value_stack.pop()
            self._value_stack[-1].append(item)
        elif operation == 'create_string':
            self._value_stack.append(token.value)
        elif operation == 'create_number':
            self._value_stack.append(float(token.value))
        elif operation == 'create_true':
            self._value_stack.append(True)
        elif operation == 'create_false':
            self._value_stack.append(False)
        elif operation == 'create_null':
            self._value_stack.append(None)

grammar = Grammar(filename='json_parser.grammar', classifier=is_in_class)
json_parser = JsonParser(grammar, json_tokenizer)
json_parser.parse()
print(json_parser.result)
\end{python}

It results the following output as expected.

\begin{verbatim}
{'first': 0.1234, 'second': [1.0, 2.0, 3.0, None]}
\end{verbatim}

\end{document}
